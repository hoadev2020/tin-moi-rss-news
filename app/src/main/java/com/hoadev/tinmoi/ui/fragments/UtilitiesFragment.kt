package com.hoadev.tinmoi.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.hoadev.tinmoi.R
import com.hoadev.tinmoi.ui.MainActivity
import com.hoadev.tinmoi.ui.NewsViewModel

class UtilitiesFragment : Fragment(R.layout.fragment_utilities) {
    lateinit var viewModel: NewsViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as MainActivity).viewModel
    }
}