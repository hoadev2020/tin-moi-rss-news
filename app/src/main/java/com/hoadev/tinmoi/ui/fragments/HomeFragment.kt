package com.hoadev.tinmoi.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.hoadev.tinmoi.R
import com.hoadev.tinmoi.adapters.NewsAdapter
import com.hoadev.tinmoi.ui.MainActivity
import com.hoadev.tinmoi.ui.NewsViewModel
import com.prof.rssparser.Parser
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment(R.layout.fragment_home) {
    lateinit var viewModel: NewsViewModel
    lateinit var newsAdapter: NewsAdapter
    private lateinit var parser: Parser

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as MainActivity).viewModel
        setupRecycleView()

        newsAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("article", it)
            }

            findNavController().navigate(
                R.id.action_homeFragment_to_articleFragment,
                bundle
            )
        }
        parser = Parser.Builder()
            .context(requireContext())
            // If you want to provide a custom charset (the default is utf-8):
            // .charset(Charset.forName("ISO-8859-7"))
            .cacheExpirationMillis(24L * 60L * 60L * 100L) // one day
            .build()

        viewModel.rssChannel.observe(this, Observer { channel ->
            channel?.let {
                newsAdapter.differ.submitList(channel.articles)
                mainProgressBar.visibility = View.GONE
            }
        })

        val isOnline = viewModel.isOnline()
        if(!isOnline) {
            Toast.makeText(requireContext(),"No internet connection",Toast.LENGTH_SHORT).show()
        }else if(isOnline) {
            viewModel.fetchFeed(parser)
        }

        viewModel.snackbar.observe(this, Observer { value ->
            value?.let {
                Snackbar.make(requireView(), value, Snackbar.LENGTH_LONG).show()
                viewModel.onSnackbarShowed()
            }
        })
    }

    private fun setupRecycleView() {
        newsAdapter = NewsAdapter()
        rvNewsHome.apply {
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }
}