package com.hoadev.tinmoi.ui.fragments

import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import com.hoadev.tinmoi.R
import com.hoadev.tinmoi.ui.MainActivity
import com.hoadev.tinmoi.ui.NewsViewModel
import kotlinx.android.synthetic.main.fragment_article.*

class ArticleFragment : Fragment(R.layout.fragment_article) {
    lateinit var viewModel: NewsViewModel

    val args : ArticleFragmentArgs by navArgs()
    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as MainActivity).viewModel

        val article = args.article
        webView.apply {
            webViewClient = WebViewClient()
            webChromeClient = WebChromeClient()
            isHorizontalScrollBarEnabled = true
            settings.javaScriptEnabled = true
            settings.loadWithOverviewMode = true
            loadDataWithBaseURL(null, "<style>img{display: inline; height: auto; max-width: 100%;} iframe{height: auto; width: auto;}</style><h2>${article.title}</h2><h4>${article.description}</h4>${article.content}", null, "utf-8", null)
        }


        fab.setOnClickListener {
            viewModel.saveArticle(article)
            Snackbar.make(view,"Article saved successfully",Snackbar.LENGTH_SHORT).show()
        }
    }
}