package com.hoadev.tinmoi.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.prof.rssparser.Article

class DatabaseHandler (
    val context: Context
) : SQLiteOpenHelper(
    context, DATABASE_NAME, null, 1
){
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("create table $TABLE_NAME ($ID INTEGER PRIMARY KEY AUTOINCREMENT, $GUID TEXT, $IMAGE TEXT, $TITLE TEXT, $DESCRIPTION TEXT, $PUBDATE TEXT, $LINK TEXT, $CONTENT TEXT)")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun insertData(article: Article) {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(GUID,article.guid)
        contentValues.put(IMAGE,article.image)
        contentValues.put(TITLE,article.title)
        contentValues.put(DESCRIPTION,article.description)
        contentValues.put(PUBDATE,article.pubDate)
        contentValues.put(LINK,article.link)
        contentValues.put(CONTENT,article.content)
        db.insert(TABLE_NAME, null, contentValues)
    }

    fun listOfArticle() : MutableList<Article> {
        val myArticleList = mutableListOf<Article>()
        val cursor = readableDatabase.query(TABLE_NAME, arrayOf(ID, GUID,IMAGE, TITLE, DESCRIPTION,
            PUBDATE, LINK, CONTENT),null, null,null,null, null)
        try {
            if(cursor.count != 0) {
                cursor.moveToFirst()
                if(cursor.count > 0) {
                    do {
                        val guid: String = cursor.getString(cursor.getColumnIndex(GUID))
                        val image: String = cursor.getString(cursor.getColumnIndex(IMAGE))
                        val title: String = cursor.getString(cursor.getColumnIndex(TITLE))
                        val description: String  = cursor.getString(cursor.getColumnIndex(
                            DESCRIPTION))
                        val pubDate: String = cursor.getString(cursor.getColumnIndex(PUBDATE))
                        val link: String = cursor.getString(cursor.getColumnIndex(LINK))
                        val content: String = cursor.getString(cursor.getColumnIndex(CONTENT))
                        val article = Article(guid,title,null,link,pubDate,description,content,image,null,null,null,null,
                            mutableListOf())
                        myArticleList.add(article)
                    } while (cursor.moveToNext())
                }
            }
        } finally {
            cursor.close()
        }
        return  myArticleList
    }

    fun deleteData(guid : String) : Int {
        val db = this.writableDatabase
        return db.delete(TABLE_NAME,"$GUID = ?", arrayOf(guid))
    }


    companion object {
        const val DATABASE_NAME = "new_db.db"
        const val TABLE_NAME = "news_table"
        const val ID = "id"
        const val GUID = "guid"
        const val IMAGE = "image"
        const val TITLE = "title"
        const val DESCRIPTION = "description"
        const val PUBDATE = "pub_date"
        const val LINK = "link"
        const val CONTENT = "content"
    }
}